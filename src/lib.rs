#![allow(clippy::missing_panics_doc, clippy::missing_errors_doc)]
use nom::{combinator::all_consuming, IResult};
use std::path::PathBuf;

pub use nom;

///
/// ```no_run
///    # use advent_of_code::Solver;
///    # use nom::{combinator::map_res, character::complete::digit1, IResult};
///
///    fn parse_fn(data: &str)-> IResult<&str, isize> { map_res(digit1, |s: &str|s.parse::<isize>())(data) }
///    fn first_fn(num: &isize) -> String { format!("{}", num) }
///    fn second_fn(num: &isize) -> String { format!("{}", num) }
///
///    Solver::with_input("res/input.txt")
///        .with_parser(parse_fn)
///        .with_first_solution(first_fn)
///        .with_second_solution(second_fn)
///        .solve()
///        .unwrap()
///
/// ```
///
pub struct Solver<I, O: ToString> {
    parsing_fn: fn(&str) -> IResult<&str, I>,
    first_fn: Option<fn(&I) -> O>,
    second_fn: Option<fn(&I) -> O>,
    input_path: PathBuf,
    debug_parser: bool,
}

impl<I: std::fmt::Debug, O: ToString> Solver<I, O> {
    /// Create a new [Solver] with the given input path.
    #[must_use]
    pub fn with_input(path: &str) -> Self {
        Self {
            input_path: path.into(),
            parsing_fn: |_| unimplemented!(),
            first_fn: None,
            second_fn: None,
            debug_parser: false,
        }
    }

    /// Specify the parsing function to use.
    /// The input data will be `trim`med and `all_consuming` will be called
    #[must_use]
    pub fn with_parser(mut self, parser: fn(&str) -> IResult<&str, I>) -> Self {
        self.parsing_fn = parser;
        self
    }

    /// Stop after parsing and print the result of the parser.
    #[must_use]
    pub fn debug_parser(mut self) -> Self {
        self.debug_parser = true;
        self
    }

    /// Specify the function to use for the first solution.
    #[must_use]
    pub fn with_first_solution(mut self, func: fn(&I) -> O) -> Self {
        self.first_fn.replace(func);
        self
    }

    /// Specify the function to use for the second solution.
    #[must_use]
    pub fn with_second_solution(mut self, func: fn(&I) -> O) -> Self {
        self.second_fn.replace(func);
        self
    }

    /// Parse the input and call the needed functions.
    pub fn solve(self) -> anyhow::Result<()> {
        let data = std::fs::read_to_string(self.input_path.clone())?;

        let parsed = all_consuming(self.parsing_fn)(data.trim_end()).unwrap().1;

        if self.debug_parser {
            println!("Parsed data: {:#?}", parsed);
            return Ok(());
        }

        let res1 = self
            .first_fn
            .map_or("not yet implemented".to_string(), |f| {
                f(&parsed).to_string()
            });
        println!("First solution: {}", res1);

        let res2 = self
            .second_fn
            .map_or("not yet implemented".to_string(), |f| {
                f(&parsed).to_string()
            });
        println!("Second solution: {}", res2);

        Ok(())
    }
}
