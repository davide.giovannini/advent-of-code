use advent_of_code::Solver;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{digit1, line_ending};
use nom::combinator::{map, opt};
use nom::multi::separated_list1;
use nom::sequence::{pair, separated_pair};
use nom::IResult;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day10.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

#[derive(Debug)]
enum Instruction {
    Noop,
    Add(isize),
}

type Input = Vec<Instruction>;

fn first_solution(input: &Input) -> isize {
    let mut x_register = 1;

    let mut signal_strength = 0;

    let mut instructions = input.iter();
    let mut instruction = None;
    let mut working = false;
    for cycle in 1.. {
        if !working {
            instruction = instruction.or_else(|| instructions.next());
        }
        if let Some(instr) = instruction {
            // -------------CYCLE ------------
            if cycle == 20 || (cycle - 20) % 40 == 0 {
                signal_strength += x_register * cycle;
            }
            match instr {
                Instruction::Noop => {
                    instruction.take();
                }
                Instruction::Add(val) => {
                    if working {
                        working = false;
                        x_register += val;
                        instruction.take();
                    } else {
                        working = true;
                    }
                }
            }
            // ------------------------------
        } else {
            break;
        }
    }

    signal_strength
}

fn second_solution(input: &Input) -> isize {
    unimplemented!()
}

fn parse_instruction(input: &str) -> IResult<&str, Instruction> {
    alt((
        map(tag("noop"), |_| Instruction::Noop),
        map(
            separated_pair(tag("addx"), tag(" "), pair(opt(tag("-")), digit1)),
            |(_, (sign, n)): (&str, (Option<&str>, &str))| {
                let mut num: isize = n.parse().unwrap();
                if sign.is_some() {
                    num = -num;
                }
                Instruction::Add(num)
            },
        ),
    ))(input)
}
fn parser(input: &str) -> IResult<&str, Input> {
    separated_list1(line_ending, parse_instruction)(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn small_example() {
        let input = r#"noop
addx 3
addx -5"#;

        let first_answer = 0;

        let parsed = parser(input).unwrap().1;

        assert_eq!(first_solution(&parsed), first_answer);
    }

    #[test]
    fn example() {
        let input = r#"addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop"#;
        let first_answer = 13140;
        let second_answer = 100;

        let parsed = parser(input).unwrap().1;

        assert_eq!(first_solution(&parsed), first_answer);
        assert_eq!(second_solution(&parsed), second_answer)
    }
}
