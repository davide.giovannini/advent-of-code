use advent_of_code::Solver;
use nom::character::complete::{alpha1, line_ending};
use nom::combinator::map_res;
use std::collections::HashSet;
use std::str::FromStr;

use nom::multi::separated_list1;
use nom::IResult;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day3.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

type Compartment = Vec<char>;
#[derive(Debug)]
struct Rucksack {
    first: Compartment,
    second: Compartment,
}

impl FromStr for Rucksack {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let chars: Vec<char> = s.chars().collect();
        let half = s.len() / 2;

        Ok(Self {
            first: chars[0..half].to_vec(),
            second: chars[half..].to_vec(),
        })
    }
}

impl Rucksack {
    fn priority_of(c: char) -> usize {
        let c_usize = c as usize;
        if c.is_lowercase() {
            c_usize - ('a' as usize) + 1
        } else {
            c_usize - ('A' as usize) + 27
        }
    }

    fn find_duplicate(&self) -> char {
        let first_set = self.first.iter().copied().collect::<HashSet<char>>();
        let second_set = self.second.iter().copied().collect::<HashSet<char>>();

        let intersection: Vec<char> = first_set.intersection(&second_set).copied().collect();
        assert_eq!(intersection.len(), 1);
        intersection[0]
    }

    fn as_set(&self) -> HashSet<char> {
        self.first
            .iter()
            .copied()
            .chain(self.second.iter().copied())
            .collect()
    }
}

type Input = Vec<Rucksack>;

fn first_solution(input: &Input) -> usize {
    input
        .iter()
        .map(|r| Rucksack::priority_of(r.find_duplicate()))
        .sum()
}

fn second_solution(input: &Input) -> usize {
    input
        .as_slice()
        .chunks(3)
        .map(|r| {
            let inter: HashSet<char> = r[0]
                .as_set()
                .intersection(&r[1].as_set())
                .copied()
                .collect();
            let common_item = inter
                .intersection(&r[2].as_set())
                .copied()
                .collect::<Vec<_>>()[0];
            Rucksack::priority_of(common_item)
        })
        .sum()
}

fn parser(input: &str) -> IResult<&str, Input> {
    separated_list1(line_ending, map_res(alpha1, str::parse::<Rucksack>))(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn priority() {
        assert_eq!(Rucksack::priority_of('a'), 1);
        assert_eq!(Rucksack::priority_of('z'), 26);
        assert_eq!(Rucksack::priority_of('A'), 27);
        assert_eq!(Rucksack::priority_of('Z'), 52);
    }

    #[test]
    fn example() {
        let input = r#"vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw"#;

        let parsed = parser(input).unwrap().1;

        assert_eq!(first_solution(&parsed), 157);
        assert_eq!(second_solution(&parsed), 70)
    }
}
