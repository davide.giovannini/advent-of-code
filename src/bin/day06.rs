use advent_of_code::Solver;
use nom::character::complete::alpha1;
use nom::combinator::map;
use std::collections::VecDeque;

use nom::IResult;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day06.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

type Input = String;

fn find_n_unique_chars(input: &str, num: usize) -> usize {
    let mut buffer = VecDeque::new();

    'main_loop: for (n, c) in input.chars().enumerate() {
        buffer.push_back(c);
        if buffer.len() > num {
            buffer.pop_front();

            for start_i in 0..(num - 1) {
                for end_i in (start_i + 1)..num {
                    if buffer[start_i] == buffer[end_i] {
                        continue 'main_loop;
                    }
                }
            }
            return n + 1;
        }
    }
    unreachable!()
}

fn first_solution(input: &Input) -> usize {
    find_n_unique_chars(input, 4)
}

fn second_solution(input: &Input) -> usize {
    find_n_unique_chars(input, 14)
}

fn parser(input: &str) -> IResult<&str, Input> {
    map(alpha1, String::from)(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let input = r#"mjqjpqmgbljsphdztnvjfqwrcgsmlb"#;
        let first_answer = 7;
        let second_answer = 19;

        let parsed = parser(input).unwrap().1;

        assert_eq!(first_solution(&parsed), first_answer);
        assert_eq!(second_solution(&parsed), second_answer)
    }
}
