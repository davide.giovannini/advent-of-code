use advent_of_code::Solver;
use nom::character::complete::{digit1, line_ending};
use nom::combinator::map_res;
use std::ops::RangeInclusive;

use nom::bytes::complete::tag;

use nom::multi::separated_list1;
use nom::sequence::separated_pair;
use nom::IResult;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day04.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

type Range = RangeInclusive<usize>;
fn range_included(first: &Range, second: &Range) -> bool {
    first.start() <= second.start() && first.end() >= second.end()
}
fn range_overlap(first: &Range, second: &Range) -> bool {
    first.contains(second.start())
}

#[derive(Debug)]
struct TaskPair {
    first: Range,
    second: Range,
}

type Input = Vec<TaskPair>;

fn first_solution(input: &Input) -> usize {
    input
        .iter()
        .filter(|t| range_included(&t.first, &t.second) || range_included(&t.second, &t.first))
        .count()
}

fn second_solution(input: &Input) -> usize {
    input
        .iter()
        .filter(|t| range_overlap(&t.first, &t.second) || range_overlap(&t.second, &t.first))
        .count()
}

fn parse_num(input: &str) -> IResult<&str, usize> {
    map_res(digit1, str::parse::<usize>)(input)
}

fn parse_range(input: &str) -> IResult<&str, Range> {
    (separated_pair(parse_num, tag("-"), parse_num)(input))
        .map(|(input, (start, end))| (input, start..=end))
}

fn parse_task(input: &str) -> IResult<&str, TaskPair> {
    (separated_pair(parse_range, tag(","), parse_range)(input))
        .map(|(input, (first, second))| (input, TaskPair { first, second }))
}

fn parser(input: &str) -> IResult<&str, Input> {
    separated_list1(line_ending, parse_task)(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let input = r#"2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8"#;
        let first_answer = 2;
        let second_answer = 4;

        let parsed = parser(input).unwrap().1;

        assert_eq!(first_solution(&parsed), first_answer);
        assert_eq!(second_solution(&parsed), second_answer)
    }
}
