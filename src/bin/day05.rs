use advent_of_code::Solver;
use nom::character::complete::{anychar, char as nchar, digit1, line_ending};
use nom::combinator::{map, map_res, opt};
use std::fmt::{Debug, Formatter};

use nom::branch::alt;
use nom::bytes::complete::tag;

use nom::multi::separated_list1;
use nom::sequence::{delimited, preceded, separated_pair, tuple};
use nom::IResult;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day05.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

type MoveInstruction = (usize, usize, usize);

#[derive(Clone)]
struct CargoStack {
    stacks: [Vec<char>; 9],
}
impl Debug for CargoStack {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "---------------------------")?;
        let mut i = 0;
        loop {
            let mut wrote = false;
            for stack in &self.stacks {
                if stack.len() > i {
                    wrote = true;
                    write!(f, "[{}] ", stack[i])?;
                } else {
                    write!(f, "    ")?;
                }
            }
            i += 1;
            writeln!(f)?;
            if !wrote {
                break;
            }
        }
        writeln!(f, "---------------------------")
    }
}

impl CargoStack {
    fn new(cargo: Vec<Vec<Option<char>>>) -> Self {
        let mut stacks: [Vec<char>; 9] = Default::default();
        for row in cargo {
            for (i, value) in row.iter().enumerate() {
                if let Some(val) = value {
                    stacks[i].push(*val);
                }
            }
        }

        for vector in &mut stacks {
            vector.reverse();
        }

        Self { stacks }
    }
}

#[derive(Debug)]
struct Input {
    cargo_stack: CargoStack,
    instructions: Vec<MoveInstruction>,
}

fn first_solution(input: &Input) -> String {
    let mut cargo = input.cargo_stack.clone();

    for (i_move, i_from, i_to) in &input.instructions {
        for _ in 0..*i_move {
            let elem = cargo.stacks[*i_from - 1].pop().unwrap();
            cargo.stacks[*i_to - 1].push(elem);
        }
    }

    cargo
        .stacks
        .iter()
        .map(|v| v.last().copied().unwrap_or('_'))
        .collect()
}

fn second_solution(input: &Input) -> String {
    let mut cargo = input.cargo_stack.clone();

    let mut buffer = Vec::new();
    for (i_move, i_from, i_to) in &input.instructions {
        for _ in 0..*i_move {
            let elem = cargo.stacks[*i_from - 1].pop().unwrap();
            buffer.push(elem);
        }
        while let Some(c) = buffer.pop() {
            cargo.stacks[*i_to - 1].push(c);
        }
    }

    cargo
        .stacks
        .iter()
        .map(|v| v.last().copied().unwrap_or('_'))
        .collect()
}

fn parse_usize(input: &str) -> IResult<&str, usize> {
    map_res(digit1, str::parse)(input)
}

fn parse_move_instr(input: &str) -> IResult<&str, MoveInstruction> {
    tuple((
        preceded(tag("move "), parse_usize),
        preceded(tag(" from "), parse_usize),
        preceded(tag(" to "), parse_usize),
    ))(input)
}

fn parse_move_instructions(input: &str) -> IResult<&str, Vec<MoveInstruction>> {
    separated_list1(line_ending, parse_move_instr)(input)
}

fn parse_cargo_element(input: &str) -> IResult<&str, Option<char>> {
    alt((
        map(tag("   "), |_| None),
        opt(delimited(nchar('['), anychar, nchar(']'))),
    ))(input)
}
fn parse_cargo_row(input: &str) -> IResult<&str, Vec<Option<char>>> {
    separated_list1(nchar(' '), parse_cargo_element)(input)
}
fn parse_cargo_data(input: &str) -> IResult<&str, Vec<Vec<Option<char>>>> {
    separated_list1(line_ending, parse_cargo_row)(input)
}

fn parser(input: &str) -> IResult<&str, Input> {
    let separator = "1   2   3   4   5   6   7   8   9 \n\n";
    map(
        separated_pair(parse_cargo_data, tag(separator), parse_move_instructions),
        |(cargo, instructions)| Input {
            cargo_stack: CargoStack::new(cargo),
            instructions,
        },
    )(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let input = r#"    [D]
[N] [C]
[Z] [M] [P]
"#
        .to_owned()
            + " 1   2   3   4   5   6   7   8   9 \n\n"
            + r#"move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2"#;
        let first_answer = "CMZ______";
        let second_answer = "MCD______";

        let parsed = parser(&input).unwrap().1;

        assert_eq!(first_solution(&parsed), first_answer);
        assert_eq!(second_solution(&parsed), second_answer)
    }

    #[test]
    fn parsing_instructions() {
        let move_instructions = r#"move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2"#;
        let parsed = parse_move_instructions(move_instructions).unwrap().1;

        assert_eq!(parsed, vec![(1, 2, 1), (3, 1, 3), (2, 2, 1), (1, 1, 2)]);
    }

    #[test]
    fn parsing_cargo() {
        let cargo = r#"    [V] [G]             [H]
[Z] [H] [Z]         [T] [S]
[P] [D] [F]         [B] [V] [Q]
[B] [M] [V] [N]     [F] [D] [N]
[Q] [Q] [D] [F]     [Z] [Z] [P] [M]
[M] [Z] [R] [D] [Q] [V] [T] [F] [R]
[D] [L] [H] [G] [F] [Q] [M] [G] [W]
[N] [C] [Q] [H] [N] [D] [Q] [M] [B]"#;
        let parsed = parse_cargo_data(cargo).unwrap().1;

        let cargo = CargoStack::new(parsed);
        assert_eq!(cargo.stacks.len(), 9);
    }
}
