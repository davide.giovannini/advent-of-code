use advent_of_code::Solver;
use nom::bytes::complete::tag;
use nom::character::complete::{digit1, line_ending};
use nom::combinator::map_res;

use nom::multi::separated_list1;
use nom::IResult;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day1.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

type Calories = Vec<usize>;
type Input = Vec<Calories>;

fn first_solution(input: &Input) -> usize {
    // Highest sum of calories
    input.iter().map(|l| l.iter().sum()).max().unwrap()
}

fn second_solution(input: &Input) -> usize {
    // Sum of the top3 calories sum
    let mut calories_sum: Vec<_> = input.iter().map(|l| l.iter().sum()).collect();
    calories_sum.sort_unstable();
    calories_sum.iter().rev().take(3).sum()
}

fn parser(input: &str) -> IResult<&str, Input> {
    let calories = separated_list1(line_ending, map_res(digit1, str::parse::<usize>));
    separated_list1(tag("\n\n"), calories)(input)
}
