use nom::{
    character::complete::digit1, character::complete::line_ending, combinator::map_res,
    multi::separated_list1,
};

use advent_of_code::Solver;

use std::collections::HashSet;

fn main() {
    Solver::with_input("res/day1.txt")
        .with_parser(input)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
        .unwrap();
}

fn first_solution(expenses_vec: &Vec<isize>) -> isize {
    let expenses: HashSet<isize> = expenses_vec.iter().cloned().collect();
    assert_eq!(expenses.len(), expenses_vec.len());

    for entry in &expenses {
        let needed = 2020 - *entry;
        if expenses.contains(&needed) {
            return entry * needed;
        }
    }
    unreachable!()
}

fn second_solution(expenses_vec: &Vec<isize>) -> isize {
    let expenses: HashSet<isize> = expenses_vec.iter().cloned().collect();
    assert_eq!(expenses.len(), expenses_vec.len());

    for first_entry in expenses_vec {
        for entry in &expenses {
            if first_entry == entry {
                continue;
            }
            let needed = 2020 - *first_entry - *entry;
            if expenses.contains(&needed) {
                return first_entry * entry * needed;
            }
        }
    }
    unreachable!()
}

fn input(data: &str) -> nom::IResult<&str, Vec<isize>> {
    (separated_list1(line_ending, map_res(digit1, |s: &str| s.parse::<isize>())))(data)
}
