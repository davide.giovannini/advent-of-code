use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{alpha1, char, digit1, line_ending, space1},
    combinator::{map, map_res},
    multi::separated_list1,
    sequence::{separated_pair, terminated},
    IResult,
};

use advent_of_code::Solver;

use std::collections::BTreeMap;

fn main() {
    Solver::with_input("res/day7.txt")
        .with_parser(parser)
        .with_first_solution(|data| count_paths_to(BagColor("shiny gold".to_string()), data))
        .with_second_solution(|data| count_quantities(BagColor("shiny gold".to_string()), data))
        .solve()
        .unwrap();
}

#[derive(Debug, Clone, PartialOrd, PartialEq, Ord, Eq)]
struct BagColor(String);

#[derive(Debug, Default, Clone, PartialOrd, PartialEq, Ord, Eq)]
struct BagInfo {
    contains: BTreeMap<BagColor, usize>,
}

fn count_paths_to(target_color: BagColor, bag_info: &BTreeMap<BagColor, BagInfo>) -> usize {
    bag_info
        .keys()
        .filter(|c| **c != target_color && can_reach_color(c, &target_color, bag_info))
        .count()
}

fn can_reach_color(
    starting: &BagColor,
    target_color: &BagColor,
    bag_info: &BTreeMap<BagColor, BagInfo>,
) -> bool {
    let mut stack = vec![starting];

    while let Some(color) = stack.pop() {
        if color == target_color {
            return true;
        } else if let Some(info) = bag_info.get(color) {
            stack.extend(info.contains.keys())
        }
    }
    false
}

fn count_quantities(starting_color: BagColor, bag_info: &BTreeMap<BagColor, BagInfo>) -> usize {
    let mut stack: Vec<_> = bag_info[&starting_color]
        .contains
        .iter()
        .map(|(c, q)| (c, *q))
        .collect();
    let mut total = 0;

    while let Some((color, q)) = stack.pop() {
        let info = &bag_info[color];

        total += q;
        stack.extend(info.contains.iter().map(|(c, qq)| (c, qq * q)))
    }

    total
}

// ----- PARSING -----

fn bag_color(input: &str) -> IResult<&str, BagColor> {
    map(
        terminated(
            separated_pair(alpha1, space1, alpha1),
            alt((tag(" bags"), tag(" bag"))),
        ),
        |a: (&str, &str)| BagColor(format!("{} {}", a.0, a.1)),
    )(input)
}

fn color_and_quantity(input: &str) -> IResult<&str, (BagColor, usize)> {
    map(
        separated_pair(map_res(digit1, str::parse), space1, bag_color),
        |(q, c)| (c, q),
    )(input)
}

fn bag_info(input: &str) -> IResult<&str, BagInfo> {
    let bag_info = map(
        separated_list1(tag(", "), color_and_quantity),
        |mut info| BagInfo {
            contains: info.drain(..).collect(),
        },
    );
    let no_bag_info = map(tag("no other bags"), |_| BagInfo::default());

    alt((bag_info, no_bag_info))(input)
}

fn parser(data: &str) -> IResult<&str, BTreeMap<BagColor, BagInfo>> {
    let row = terminated(
        separated_pair(bag_color, tag(" contain "), bag_info),
        char('.'),
    );

    map(separated_list1(line_ending, row), |mut r| {
        r.drain(..).collect()
    })(data)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parser() {
        let row = "light red bags contain 1 bright white bag, 2 muted yellow bags.";

        let expected: BTreeMap<BagColor, BagInfo> = vec![(
            BagColor("light red".to_string()),
            BagInfo {
                contains: vec![
                    (BagColor("bright white".to_string()), 1),
                    (BagColor("muted yellow".to_string()), 2),
                ]
                .drain(..)
                .collect(),
            },
        )]
        .drain(..)
        .collect();

        assert_eq!(expected, parser(row).unwrap().1)
    }
    #[test]
    fn test_parser_no_other_bags() {
        let row = "light red bags contain no other bags.";

        let expected: BTreeMap<BagColor, BagInfo> =
            vec![(BagColor("light red".to_string()), BagInfo::default())]
                .drain(..)
                .collect();

        assert_eq!(expected, parser(row).unwrap().1)
    }

    #[test]
    fn test_case_1() {
        let input = r#"light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags."#;

        let data = parser(input).unwrap().1;

        let count = count_paths_to(BagColor("shiny gold".to_string()), &data);
        assert_eq!(count, 4);

        // second solution

        assert_eq!(
            32,
            count_quantities(BagColor("shiny gold".to_string()), &data)
        )
    }
    #[test]
    fn test_case_2() {
        let input = r#"shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.
"#;

        let data = parser(input).unwrap().1;

        assert_eq!(
            126,
            count_quantities(BagColor("shiny gold".to_string()), &data)
        )
    }
}
