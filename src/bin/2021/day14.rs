use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, digit1, line_ending},
    combinator::{map, map_res},
    multi::{count, separated_list1},
    sequence::{preceded, separated_pair},
    IResult,
};

use advent_of_code::Solver;

use std::collections::HashMap;
use std::num::ParseIntError;
use std::str::FromStr;

fn main() {
    Solver::with_input("res/day14.txt")
        .with_parser(parser)
        .with_first_solution(process_instructions)
        .solve()
        .unwrap();
}

fn process_instructions(instructions: &Vec<Instruction>) -> u64 {
    let mut memory = HashMap::new();
    let mut bitmask = BitMask::default();

    for instruction in instructions {
        match instruction {
            Instruction::SetBitmask(mask) => bitmask = *mask,
            Instruction::UpdateValue { address, value } => {
                memory.insert(*address, bitmask | *value);
            }
        }
    }
    memory.values().sum()
}

#[derive(Default, Copy, Clone)]
struct BitMask {
    mask_1: u64,
    mask_0: u64,
}

impl std::fmt::Debug for BitMask {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(
            f,
            "BitMask{{\n\t{:36b}\n\t{:36b}\n}}",
            self.mask_0, self.mask_1
        )
    }
}

impl std::ops::BitOr<u64> for BitMask {
    type Output = u64;

    fn bitor(self, rhs: u64) -> Self::Output {
        (rhs | self.mask_0) & self.mask_1
    }
}

impl FromStr for BitMask {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mask_0 = u64::from_str_radix(&s.replace('X', "0"), 2)?;
        let mask_1 = u64::from_str_radix(&s.replace('X', "1"), 2)?;
        Ok(BitMask { mask_0, mask_1 })
    }
}

#[derive(Debug)]
enum Instruction {
    SetBitmask(BitMask),
    UpdateValue { address: u64, value: u64 },
}

fn parse_u64(input: &str) -> IResult<&str, u64> {
    map_res(digit1, str::parse)(input)
}

fn parser(data: &str) -> IResult<&str, Vec<Instruction>> {
    let parse_bitmask = map_res(
        preceded(
            tag("mask = "),
            count(alt((char('X'), char('0'), char('1'))), 36),
        ),
        |s| BitMask::from_str(&s.into_iter().collect::<String>()).map(Instruction::SetBitmask),
    );

    let parse_update = map(
        preceded(
            tag("mem["),
            separated_pair(parse_u64, tag("] = "), parse_u64),
        ),
        |(address, value)| Instruction::UpdateValue { address, value },
    );

    separated_list1(line_ending, alt((parse_update, parse_bitmask)))(data)
}
