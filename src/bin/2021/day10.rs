use nom::{
    character::complete::{digit1, line_ending},
    combinator::{map, map_res},
    multi::separated_list1,
    IResult,
};

use advent_of_code::Solver;
use std::collections::HashMap;

fn main() {
    Solver::with_input("res/day10.txt")
        .with_parser(parser)
        .with_first_solution(|d| {
            let deltas = d.count_jolt_deltas();

            deltas[&1] * deltas[&3]
        })
        .solve()
        .unwrap();
}

type NumType = usize;

#[derive(Debug)]
struct DataStream(Vec<NumType>);

impl DataStream {
    fn count_jolt_deltas(&self) -> HashMap<usize, usize> {
        let mut current = 0;
        let mut result = HashMap::default();

        for jolt in &self.0 {
            let delta = *jolt - current;
            assert!(delta <= 3);
            current = *jolt;

            *(result.entry(delta).or_default()) += 1;
        }

        // add final jolt delta
        *(result.entry(3).or_default()) += 1;

        result
    }
}

// ------------- PARSING -----------------

fn parse_num(input: &str) -> IResult<&str, NumType> {
    map_res(digit1, str::parse)(input)
}

fn parser(data: &str) -> IResult<&str, DataStream> {
    map(separated_list1(line_ending, parse_num), |mut v| {
        v.sort_unstable();
        DataStream(v)
    })(data)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_case() {
        let input = r#"16
10
15
5
1
11
7
19
6
12
4"#;

        let data = parser(input).unwrap().1;

        let deltas = data.count_jolt_deltas();
        println!("{:?}", deltas);
        assert_eq!((deltas[&1] * deltas[&3]), 35);
    }
    #[test]
    fn test_case2() {
        let input = r#"28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3"#;

        let data = parser(input).unwrap().1;

        let deltas = data.count_jolt_deltas();
        println!("{:?}", deltas);
        assert_eq!((deltas[&1] * deltas[&3]), 220);
    }
}
