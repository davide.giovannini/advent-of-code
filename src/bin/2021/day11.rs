use nom::{
    branch::alt,
    character::complete::{char, line_ending},
    combinator::map,
    multi::many1,
    multi::separated_list1,
    IResult,
};

use advent_of_code::Solver;

fn main() {
    Solver::with_input("res/day11.txt")
        .with_parser(parser)
        .with_first_solution(|d| d.count_seat_when_stable(4, Map::check_occupied_neighbours))
        .with_second_solution(|d| d.count_seat_when_stable(5, Map::check_visible_seats))
        .solve()
        .unwrap();
}

#[derive(Debug, Copy, Eq, PartialEq, Clone)]
enum GridElem {
    Floor,
    EmptySeat,
    OccupiedSeat,
}

#[derive(Debug, Clone)]
struct Map {
    data: Vec<Vec<GridElem>>,
    width: usize,
    height: usize,
}

impl Map {
    fn count_seat_when_stable(
        &self,
        leave_threshold: usize,
        check_fn: fn(&Map, isize, isize) -> usize,
    ) -> usize {
        let mut changed;

        let mut buffer1 = self.clone();
        let mut buffer = self.clone();
        let mut pointer = false;

        loop {
            changed = false;

            let (read, write) = if pointer {
                (&mut buffer1, &mut buffer)
            } else {
                (&mut buffer, &mut buffer1)
            };

            for x in 0..self.width {
                for y in 0..self.height {
                    let elem = read.data[y][x];
                    write.data[y][x] = elem;

                    match elem {
                        GridElem::Floor => continue,
                        GridElem::EmptySeat => {
                            if check_fn(read, x as _, y as _) == 0 {
                                write.data[y][x] = GridElem::OccupiedSeat;
                                changed = true;
                            }
                        }
                        GridElem::OccupiedSeat => {
                            if check_fn(read, x as _, y as _) >= leave_threshold {
                                write.data[y][x] = GridElem::EmptySeat;
                                changed = true;
                            }
                        }
                    }
                }
            }

            pointer = !pointer;

            if !changed {
                return write
                    .data
                    .iter()
                    .flatten()
                    .cloned()
                    .filter(|g| *g == GridElem::OccupiedSeat)
                    .count();
            }
        }
    }

    fn check_occupied_neighbours(&self, x: isize, y: isize) -> usize {
        self.count_seat(x + 1, y)
            + self.count_seat(x - 1, y)
            + self.count_seat(x, y + 1)
            + self.count_seat(x, y - 1)
            + self.count_seat(x + 1, y + 1)
            + self.count_seat(x + 1, y - 1)
            + self.count_seat(x - 1, y + 1)
            + self.count_seat(x - 1, y - 1)
    }

    fn count_seat(&self, x: isize, y: isize) -> usize {
        let out_of_bounds = x < 0 || y < 0 || x >= self.width as _ || y >= self.height as _;

        if !out_of_bounds && self.data[y as usize][x as usize] == GridElem::OccupiedSeat {
            1
        } else {
            0
        }
    }

    // Part 2
    fn check_visible_seats(&self, x: isize, y: isize) -> usize {
        self.travel_until_seat(x, y, (1, 0))
            + self.travel_until_seat(x, y, (-1, 0))
            + self.travel_until_seat(x, y, (0, 1))
            + self.travel_until_seat(x, y, (0, -1))
            + self.travel_until_seat(x, y, (1, 1))
            + self.travel_until_seat(x, y, (1, -1))
            + self.travel_until_seat(x, y, (-1, 1))
            + self.travel_until_seat(x, y, (-1, -1))
    }

    fn travel_until_seat(&self, x: isize, y: isize, direction: (isize, isize)) -> usize {
        let mut x = x;
        let mut y = y;
        loop {
            x += direction.0;
            y += direction.1;

            if x < 0 || x >= self.width as _ || y < 0 || y >= self.height as _ {
                return 0;
            }

            match self.data[y as usize][x as usize] {
                GridElem::Floor => continue,
                GridElem::EmptySeat => return 0,
                GridElem::OccupiedSeat => return 1,
            }
        }
    }
}

fn parser(data: &str) -> IResult<&str, Map> {
    let parse_elem = alt((
        map(char('.'), |_| GridElem::Floor),
        map(char('#'), |_| GridElem::OccupiedSeat),
        map(char('L'), |_| GridElem::EmptySeat),
    ));
    let row = many1(parse_elem);

    let columns = separated_list1(line_ending, row);
    map(columns, |data| {
        let width = data[0].len();
        let height = data.len();
        Map {
            width,
            height,
            data,
        }
    })(data)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_case() {
        let input = r#"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL"#;

        let parsed = parser(input).unwrap().1;

        assert_eq!(
            parsed.count_seat_when_stable(4, Map::check_occupied_neighbours),
            37
        );
        assert_eq!(
            parsed.count_seat_when_stable(5, Map::check_visible_seats),
            26
        )
    }
}
