use nom::{
    branch::alt,
    character::complete::{alpha1, alphanumeric1, char, hex_digit1, line_ending},
    combinator::map,
    multi::count,
    multi::separated_list1,
    sequence::{preceded, separated_pair},
    IResult,
};

use advent_of_code::Solver;

use nom::bytes::complete::tag;
use nom::character::complete::digit1;
use nom::combinator::{all_consuming, map_res};
use nom::sequence::tuple;
use std::collections::HashMap;
use std::ops::RangeInclusive;

fn main() {
    Solver::with_input("res/day4.txt")
        .with_parser(parser)
        .with_first_solution(|passports| passports.iter().filter(|p| p.has_all_fields()).count())
        .with_second_solution(|passports| {
            passports
                .iter()
                .filter(|p| p.has_all_fields() && p.has_valid_fields())
                .count()
        })
        .solve()
        .unwrap();
}

#[derive(Debug)]
struct Passport {
    fields: HashMap<String, String>,
}

impl Passport {
    fn has_all_fields(&self) -> bool {
        ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
            .iter()
            .all(|k| self.fields.contains_key(&k.to_string()))
    }

    fn has_valid_fields(&self) -> bool {
        self.fields.iter().all(|(k, v)| match k.as_str() {
            "byr" => validate_year(v, 1920..=2002),
            "iyr" => validate_year(v, 2010..=2020),
            "eyr" => validate_year(v, 2020..=2030),
            "pid" => v.len() == 9 && v.chars().all(|c| c.is_numeric()),
            "ecl" => ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&v.as_str()),
            "hcl" => validate_hair_color(v).is_ok(),
            "hgt" => validate_height(v)
                .map(|(_, (h, r))| r.contains(&h))
                .unwrap_or(false),
            "cid" => true,
            _ => unreachable!(),
        })
    }
}

fn validate_year(year: &str, range: RangeInclusive<usize>) -> bool {
    year.parse()
        .map(|n: usize| range.contains(&n))
        .unwrap_or(false)
}

fn validate_hair_color(color: &str) -> IResult<&str, bool> {
    all_consuming(map(preceded(char('#'), hex_digit1), |d: &str| d.len() == 6))(color)
}

fn validate_height(height: &str) -> IResult<&str, (usize, RangeInclusive<usize>)> {
    let num = map_res(digit1, str::parse);
    let unit = alt((map(tag("cm"), |_| 150..=193), map(tag("in"), |_| 59..=76)));

    all_consuming(tuple((num, unit)))(height)
}

fn parser(data: &str) -> IResult<&str, Vec<Passport>> {
    let entry_separator = alt((map(line_ending, |_| ' '), char(' ')));

    let key = map(alpha1, str::to_string);
    let value = alt((
        map(alphanumeric1, str::to_string),
        map(preceded(char('#'), alphanumeric1), |s| format!("#{}", s)),
    ));

    let key_value = separated_pair(key, char(':'), value);

    let passport = separated_list1(entry_separator, key_value);

    let passport_separator = count(line_ending, 2);

    map(separated_list1(passport_separator, passport), |p| {
        p.iter()
            .map(|e| Passport {
                fields: e.iter().cloned().collect(),
            })
            .collect()
    })(data)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_validate_year() {
        assert!(!validate_year("", 1920..=2020));
        assert!(validate_year("1920", 1920..=2020));
        assert!(validate_year("1980", 1920..=2020));
        assert!(validate_year("2020", 1920..=2020));
        assert!(!validate_year("2021", 1920..=2020));
        assert!(!validate_year("1934ads", 1920..=2020));
    }
}
