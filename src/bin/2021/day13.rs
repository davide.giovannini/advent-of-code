use nom::{
    branch::alt,
    character::complete::{char, digit1, line_ending},
    combinator::{map, map_res},
    multi::separated_list1,
    sequence::separated_pair,
    IResult,
};

use advent_of_code::Solver;

fn main() {
    Solver::with_input("res/day13.txt")
        .with_parser(parser)
        .with_first_solution(|i| i.first_sol())
        .solve()
        .unwrap();
}

#[derive(Debug)]
enum BusId {
    Num(usize),
    X,
}

#[derive(Debug)]
struct Input {
    arriving_time: usize,
    bus_lines: Vec<BusId>,
}

impl Input {
    fn first_sol(&self) -> usize {
        let mut best_candidate = usize::MAX;
        let mut bus_id = 0;
        let ftime = self.arriving_time as f32;

        for num in self.bus_lines.iter().filter_map(|e| match e {
            BusId::Num(num) => Some(*num as f32),
            BusId::X => None,
        }) {
            let candidate = (ftime / num).ceil() as usize * num as usize;

            if (self.arriving_time..best_candidate).contains(&candidate) {
                best_candidate = candidate;
                bus_id = num as usize;
            }
        }

        let waiting_time = best_candidate - self.arriving_time;

        waiting_time * bus_id
    }
}

fn parse_usize(input: &str) -> IResult<&str, usize> {
    map_res(digit1, str::parse)(input)
}

fn parser(data: &str) -> IResult<&str, Input> {
    let bus_id = alt((map(char('x'), |_| BusId::X), map(parse_usize, BusId::Num)));

    let bus_lines = separated_list1(char(','), bus_id);

    map(
        separated_pair(parse_usize, line_ending, bus_lines),
        |(time, lines)| Input {
            arriving_time: time,
            bus_lines: lines,
        },
    )(data)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_case() {
        let input = r#"939
7,13,x,x,59,x,31,19"#;

        let data = parser(input).unwrap().1;

        assert_eq!(data.first_sol(), 295);
    }
}
