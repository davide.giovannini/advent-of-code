use nom::{
    character::complete::{alpha1, line_ending},
    combinator::map,
    multi::count,
    multi::separated_list1,
    IResult,
};

use advent_of_code::Solver;

use std::collections::HashSet;

fn main() {
    Solver::with_input("res/day6.txt")
        .with_parser(parser)
        .with_first_solution(|groups| {
            groups
                .iter()
                .map(|g| g.iter().flatten().collect::<HashSet<_>>().len())
                .sum::<usize>()
        })
        .with_second_solution(second)
        .solve()
        .unwrap();
}

type Answers = Vec<char>;
type GroupAnswers = Vec<Answers>;

fn parser(data: &str) -> IResult<&str, Vec<GroupAnswers>> {
    let answers = map(alpha1, |s: &str| s.chars().collect());

    let group = separated_list1(line_ending, answers);

    separated_list1(count(line_ending, 2), group)(data)
}

fn second(groups: &Vec<GroupAnswers>) -> usize {
    groups
        .iter()
        .map(|group| {
            let first_elem = group[0].iter().cloned().collect::<HashSet<_>>();
            group
                .iter()
                .map(|answers| answers.iter().cloned().collect::<HashSet<char>>())
                .fold(first_elem, |a, b| a.intersection(&b).cloned().collect())
                .len()
        })
        .sum::<usize>()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_case() {
        let data_str = r#"abc

a
b
c

ab
ac

a
a
a
a

b"#;
        let data = parser(data_str).unwrap().1;

        assert_eq!(second(&data), 6);
    }
}
