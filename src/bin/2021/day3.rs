use nom::{
    branch::alt,
    character::complete::char,
    character::complete::line_ending,
    combinator::map,
    multi::{many1, separated_list1},
    IResult,
};

use advent_of_code::Solver;

fn main() {
    Solver::with_input("res/day3.txt")
        .with_parser(input)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
        .unwrap();
}

fn first_solution(map: &Vec<Vec<bool>>) -> usize {
    check_slope(3, 1, map)
}

fn second_solution(map: &Vec<Vec<bool>>) -> usize {
    [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        .iter()
        .map(|(dx, dy)| check_slope(*dx, *dy, map))
        .product()
}

fn check_slope(delta_x: usize, delta_y: usize, map: &Vec<Vec<bool>>) -> usize {
    let mut tree_count = 0;

    let mut x = 0;
    let mut y = 0;

    loop {
        y += delta_y;
        if y >= map.len() {
            break;
        }
        x = (x + delta_x) % map[y].len();

        if map[y][x] {
            tree_count += 1
        }
    }
    tree_count
}

type Map = Vec<Vec<bool>>;

fn input(data: &str) -> IResult<&str, Map> {
    let bool = map(alt((char('.'), char('#'))), |c| c == '#');
    separated_list1(line_ending, many1(bool))(data)
}
