use nom::{
    branch::alt,
    character::complete::{char, line_ending},
    combinator::map,
    multi::count,
    multi::separated_list1,
    sequence::tuple,
    IResult,
};

use advent_of_code::Solver;

use std::collections::BTreeSet;
use std::convert::TryInto;

fn main() {
    Solver::with_input("res/day5.txt")
        .with_parser(parser)
        .with_first_solution(|boarding_passes| {
            boarding_passes
                .iter()
                .map(BoardingPass::seat_id)
                .max()
                .unwrap()
        })
        .with_second_solution(find_missing_seat)
        .solve()
        .unwrap();
}

fn find_missing_seat(boarding_passes: &Vec<BoardingPass>) -> usize {
    let ids: BTreeSet<_> = boarding_passes.iter().map(BoardingPass::seat_id).collect();

    for id in ids.iter() {
        let candidate = *id + 1;
        let after_candidate = *id + 2;
        if !ids.contains(&candidate) && ids.contains(&after_candidate) {
            return candidate;
        }
    }
    unreachable!()
}

#[derive(Debug, Copy, Clone)]
enum RowIndication {
    Front,
    Back,
}

#[derive(Debug, Copy, Clone)]
enum SeatIndication {
    Left,
    Right,
}

#[derive(Debug, Copy, Clone)]
struct BoardingPass {
    row: [RowIndication; 7],
    seat: [SeatIndication; 3],
}

impl BoardingPass {
    fn row(&self) -> usize {
        let mut min = 0;
        let mut max = 127;

        for i in &self.row {
            let half = (max - min) / 2;
            match i {
                RowIndication::Front => {
                    if half > 0 {
                        max -= half + 1;
                    } else {
                        return min;
                    }
                }
                RowIndication::Back => {
                    if half > 0 {
                        min += half + 1;
                    } else {
                        return max;
                    }
                }
            }
        }
        unreachable!()
    }

    fn seat(&self) -> usize {
        let mut min = 0;
        let mut max = 7;

        for i in &self.seat {
            let half = (max - min) / 2;
            match i {
                SeatIndication::Left => {
                    if half > 0 {
                        max -= half + 1;
                    } else {
                        return min;
                    }
                }
                SeatIndication::Right => {
                    if half > 0 {
                        min += half + 1;
                    } else {
                        return max;
                    }
                }
            }
        }
        unreachable!()
    }

    fn seat_id(&self) -> usize {
        self.row() * 8 + self.seat()
    }
}

fn parser(data: &str) -> IResult<&str, Vec<BoardingPass>> {
    let row_indication = alt((
        map(char('F'), |_| RowIndication::Front),
        map(char('B'), |_| RowIndication::Back),
    ));
    let seat_indication = alt((
        map(char('L'), |_| SeatIndication::Left),
        map(char('R'), |_| SeatIndication::Right),
    ));

    let boarding_pass = map(
        tuple((count(row_indication, 7), count(seat_indication, 3))),
        |(row, seat)| BoardingPass {
            row: row.as_slice().try_into().unwrap(),
            seat: seat.as_slice().try_into().unwrap(),
        },
    );

    separated_list1(line_ending, boarding_pass)(data)
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn check_row() {
        validate("FBFBBFFRLR", 44, 5, 357);
        validate("BFFFBBFRRR", 70, 7, 567);
        validate("FFFBBBFRRR", 14, 7, 119);
        validate("BBFFBBFRLL", 102, 4, 820);
    }

    fn validate(pass: &str, row: usize, seat: usize, seat_id: usize) {
        let pass = parser(pass).unwrap().1[0];
        assert_eq!(pass.row(), row);
        assert_eq!(pass.seat(), seat);
        assert_eq!(pass.seat_id(), seat_id);
    }
}
