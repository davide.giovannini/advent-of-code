use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, digit1, line_ending, space1},
    combinator::{map, map_res},
    multi::separated_list1,
    sequence::separated_pair,
    IResult,
};

use advent_of_code::Solver;
use nom::sequence::tuple;

fn main() {
    Solver::with_input("res/day8.txt")
        .with_parser(parser)
        .with_first_solution(find_loop)
        .solve()
        .unwrap();
}

#[derive(Debug, Clone, PartialOrd, PartialEq, Ord, Eq)]
enum Instruction {
    Nop,
    Acc(isize),
    Jmp(isize),
}

fn find_loop(program: &Vec<Instruction>) -> isize {
    let mut loop_check = vec![false; program.len()];

    let mut ipointer = 0;
    let mut acc = 0;

    loop {
        if loop_check[ipointer] {
            return acc;
        }
        loop_check[ipointer] = true;

        match program[ipointer] {
            Instruction::Nop => {
                ipointer += 1;
            }
            Instruction::Acc(n) => {
                ipointer += 1;
                acc += n;
            }
            Instruction::Jmp(offs) => {
                ipointer = (ipointer as isize + offs) as usize;
            }
        }
    }
}

// ------------- PARSING -----------------

fn parse_isize_with_sign(input: &str) -> IResult<&str, isize> {
    map_res(
        tuple((alt((char('-'), char('+'))), digit1)),
        |(sign, num): (char, &str)| format!("{}{}", sign, num).parse::<isize>(),
    )(input)
}

fn parse_instruction_id(input: &str) -> IResult<&str, Box<dyn Fn(isize) -> Instruction>> {
    let nop = map(tag("nop"), |_| {
        Box::new(|_: isize| Instruction::Nop) as Box<dyn Fn(isize) -> Instruction>
    });
    let acc = map(tag("acc"), |_| {
        Box::new(Instruction::Acc) as Box<dyn Fn(isize) -> Instruction>
    });
    let jmp = map(tag("jmp"), |_| {
        Box::new(Instruction::Jmp) as Box<dyn Fn(isize) -> Instruction>
    });

    alt((nop, acc, jmp))(input)
}

fn parser(data: &str) -> IResult<&str, Vec<Instruction>> {
    let instruction = map(
        separated_pair(parse_instruction_id, space1, parse_isize_with_sign),
        |(f, n)| f(n),
    );

    separated_list1(line_ending, instruction)(data)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_parser() {
        assert_eq!(4, parse_isize_with_sign("+4").unwrap().1);
        assert_eq!(-3, parse_isize_with_sign("-3").unwrap().1)
    }
}
