use nom::{
    character::complete::alpha1,
    character::complete::char,
    character::complete::digit1,
    character::complete::line_ending,
    combinator::{map, map_res},
    multi::separated_list1,
    sequence::{separated_pair, terminated},
    IResult,
};

use advent_of_code::Solver;

fn main() {
    Solver::with_input("res/day2.txt")
        .with_parser(input)
        .with_first_solution(first)
        .with_second_solution(second)
        .solve()
        .unwrap();
}

fn first(passwords: &Vec<Password>) -> usize {
    passwords.iter().filter(|p| p.is_valid_easy()).count()
}

fn second(passwords: &Vec<Password>) -> usize {
    passwords.iter().filter(|p| p.is_valid_hard()).count()
}

#[derive(Debug)]
struct PasswordPolicy {
    range: std::ops::RangeInclusive<usize>,
    letter: char,
}

#[derive(Debug)]
struct Password {
    policy: PasswordPolicy,
    password: String,
}

impl Password {
    fn is_valid_easy(&self) -> bool {
        let letter_count = self
            .password
            .chars()
            .filter(|c| *c == self.policy.letter)
            .count();
        self.policy.range.contains(&letter_count)
    }
    fn is_valid_hard(&self) -> bool {
        let letters = self.password.as_bytes();
        let first_idx = self.policy.range.start() - 1;
        let second_idx = self.policy.range.end() - 1;

        (letters[first_idx] == self.policy.letter as u8)
            ^ (letters[second_idx] == self.policy.letter as u8)
    }
}

fn input(data: &str) -> IResult<&str, Vec<Password>> {
    let range = map(
        separated_pair(parse_usize, char('-'), parse_usize),
        |(low, high)| low..=high,
    );

    let policy = map(
        separated_pair(
            range,
            char(' '),
            terminated(map_res(alpha1, str::parse::<char>), char(':')),
        ),
        |(range, letter)| PasswordPolicy { range, letter },
    );

    let password = map(
        separated_pair(policy, char(' '), alpha1),
        |(policy, password)| Password {
            policy,
            password: password.to_string(),
        },
    );

    separated_list1(line_ending, password)(data)
}

fn parse_usize(data: &str) -> IResult<&str, usize> {
    map_res(digit1, str::parse::<usize>)(data)
}
