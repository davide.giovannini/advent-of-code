use nom::{
    character::complete::{digit1, line_ending},
    combinator::{map, map_res},
    multi::separated_list1,
    IResult,
};

use advent_of_code::Solver;

fn main() {
    Solver::with_input("res/day9.txt")
        .with_parser(parser)
        .with_first_solution(|d| d.find_invalid_stream())
        .with_second_solution(|d| {
            let num = d.find_invalid_stream();
            d.find_range(num)
        })
        .solve()
        .unwrap();
}

type NumType = i128;

#[derive(Debug)]
struct DataStream(Vec<NumType>);

impl DataStream {
    fn is_valid(&self, n: NumType, preamble_range: std::ops::Range<usize>) -> bool {
        let mut preamble: Vec<_> = self.0[preamble_range].to_vec();
        preamble.sort_unstable();

        for num1 in &preamble {
            let target = n - *num1;

            if target != *num1 && preamble.binary_search(&target).is_ok() {
                return true;
            }
        }
        false
    }

    fn find_invalid_stream(&self) -> NumType {
        for (i, num) in self.0.iter().enumerate().skip(25) {
            if !self.is_valid(*num, i - 25..i) {
                return *num;
            }
        }
        unreachable!()
    }

    /// Find contiguous range (len>=2) that sums up to num
    fn find_range(&self, num: NumType) -> NumType {
        for start in 0..self.0.len() {
            for end in (start + 2)..self.0.len() {
                let mut min = NumType::MAX;
                let mut max = NumType::MIN;
                let mut sum = 0;

                for n in &self.0[start..end] {
                    sum += n;
                    min = min.min(*n);
                    max = max.max(*n);
                }
                if sum == num {
                    return min + max;
                }
            }
        }
        unreachable!()
    }
}

// ------------- PARSING -----------------

fn parse_num(input: &str) -> IResult<&str, NumType> {
    map_res(digit1, str::parse)(input)
}

fn parser(data: &str) -> IResult<&str, DataStream> {
    map(separated_list1(line_ending, parse_num), DataStream)(data)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_case() {
        let input = r#"20
16
13
6
24
15
5
22
19
12
9
21
23
2
1
8
4
7
11
18
14
3
17
10
25
26
49
100
50"#;

        let data = parser(input).unwrap().1;

        assert!(data.is_valid(26, 0..25));
        assert!(data.is_valid(49, 0..25));
        assert!(!data.is_valid(100, 0..25));
        assert!(!data.is_valid(50, 0..25));
        assert_eq!(data.find_invalid_stream(), 100)
    }

    #[test]
    fn test_case2() {
        let input = r#"35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576"#;

        let data = parser(input).unwrap().1;

        assert_eq!(data.find_range(127), 62);
    }
}
