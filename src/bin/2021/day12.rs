use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, digit1, line_ending},
    combinator::{map, map_res},
    multi::separated_list1,
    sequence::tuple,
    IResult,
};

use advent_of_code::Solver;

fn main() {
    Solver::with_input("res/day12.txt")
        .with_parser(parser)
        .with_first_solution(|i| {
            let mut ship = Ship::default();
            ship.follow_instructions(i);

            ship.manhattan_distance()
        })
        .with_second_solution(|i| {
            let mut ship = Ship::default();
            ship.follow_instructions_with_waypoint(i);
            ship.manhattan_distance()
        })
        .solve()
        .unwrap();
}

#[derive(Debug, Default, Copy, Clone, Eq, PartialEq)]
struct Point {
    x: isize,
    y: isize,
}

impl Point {
    pub fn manhattan_distance(&self) -> isize {
        self.x.abs() + self.y.abs()
    }

    pub fn rotate_by(self, angle: Rotation, clockwise: bool) -> Self {
        let res = if clockwise {
            Point {
                x: self.y,
                y: -self.x,
            }
        } else {
            Point {
                x: -self.y,
                y: self.x,
            }
        };

        match angle {
            Rotation::R90 => res,
            Rotation::R180 => res.rotate_by(Rotation::R90, clockwise),
            Rotation::R270 => res.rotate_by(Rotation::R180, clockwise),
        }
    }
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::ops::AddAssign for Point {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl std::ops::Mul<isize> for Point {
    type Output = Point;

    fn mul(self, rhs: isize) -> Self::Output {
        Point {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

#[derive(Debug, Default)]
struct Ship {
    position: Point,
    facing: Direction,
}

impl Ship {
    pub fn follow_instructions(&mut self, instructions: &[Instruction]) {
        for i in instructions {
            match i {
                Instruction::Move(dir, dist) => {
                    self.position += dir.by(*dist);
                }
                Instruction::MoveForward(dist) => {
                    self.position += self.facing.by(*dist);
                }
                Instruction::Rotate(angle, clockwise) => {
                    self.facing = self.facing.rotate_by(*angle, *clockwise);
                }
            }
        }
    }
    pub fn follow_instructions_with_waypoint(&mut self, instructions: &[Instruction]) {
        let mut waypoint = Point { x: 10, y: 1 };

        for i in instructions {
            match i {
                Instruction::Move(dir, dist) => {
                    waypoint += dir.by(*dist);
                }
                Instruction::MoveForward(dist) => {
                    self.position += waypoint * *dist;
                }
                Instruction::Rotate(angle, clockwise) => {
                    waypoint = waypoint.rotate_by(*angle, *clockwise);
                }
            }
        }
    }

    pub fn manhattan_distance(&self) -> isize {
        self.position.manhattan_distance()
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Direction {
    North,
    East,
    West,
    South,
}

impl Default for Direction {
    fn default() -> Self {
        Direction::East
    }
}

impl Direction {
    fn by(&self, dist: isize) -> Point {
        match self {
            Direction::North => Point { x: 0, y: dist },
            Direction::East => Point { x: dist, y: 0 },
            Direction::West => Point { x: -dist, y: 0 },
            Direction::South => Point { x: 0, y: -dist },
        }
    }

    fn rotate_by(&self, angle: Rotation, clockwise: bool) -> Self {
        let dirs = [
            Direction::North,
            Direction::East,
            Direction::South,
            Direction::West,
        ];
        let idx = dirs.iter().position(|e| e == self).unwrap() as isize;

        let sign: isize = if clockwise { 1 } else { -1 };
        let new_idx = match angle {
            Rotation::R90 => 1 * sign,
            Rotation::R180 => 2 * sign,
            Rotation::R270 => 3 * sign,
        };
        let new_idx = (idx + new_idx + dirs.len() as isize) as usize % dirs.len();

        dirs[new_idx]
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Rotation {
    R90,
    R270,
    R180,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Instruction {
    Move(Direction, isize),
    MoveForward(isize),
    Rotate(Rotation, bool),
}

fn parse_isize(input: &str) -> IResult<&str, isize> {
    map_res(digit1, str::parse)(input)
}

fn parser(data: &str) -> IResult<&str, Vec<Instruction>> {
    let parse_direction = alt((
        map(char('N'), |_| Direction::North),
        map(char('W'), |_| Direction::West),
        map(char('E'), |_| Direction::East),
        map(char('S'), |_| Direction::South),
    ));

    let parse_move = alt((
        map(tuple((parse_direction, parse_isize)), |(dir, num)| {
            Instruction::Move(dir, num)
        }),
        map(tuple((char('F'), parse_isize)), |(_, num)| {
            Instruction::MoveForward(num)
        }),
    ));

    let parse_rotation = alt((map(char('R'), |_| true), map(char('L'), |_| false)));
    let parse_angle = alt((
        map(tag("90"), |_| Rotation::R90),
        map(tag("180"), |_| Rotation::R180),
        map(tag("270"), |_| Rotation::R270),
    ));

    let parse_rot_instruction = map(tuple((parse_rotation, parse_angle)), |(b, angle)| {
        Instruction::Rotate(angle, b)
    });

    let instruction = alt((parse_move, parse_rot_instruction));

    separated_list1(line_ending, instruction)(data)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_rotation() {
        assert_eq!(
            Direction::North.rotate_by(Rotation::R90, true),
            Direction::East
        );
        assert_eq!(
            Direction::North.rotate_by(Rotation::R90, false),
            Direction::West
        );
        assert_eq!(
            Direction::North.rotate_by(Rotation::R180, true),
            Direction::South
        );
        assert_eq!(
            Direction::North.rotate_by(Rotation::R180, false),
            Direction::South
        );
    }

    #[test]
    fn test_point_rotation() {
        let point = Point { x: 3, y: -5 };
        assert_eq!(point.rotate_by(Rotation::R90, true), Point { x: -5, y: -3 });
        assert_eq!(point.rotate_by(Rotation::R180, true), Point { x: -3, y: 5 });
    }

    #[test]
    fn test_case() {
        let input = r#"F10
N3
F7
R90
F11"#;

        let inst = parser(input).unwrap().1;

        let mut ship = Ship::default();
        ship.follow_instructions(&inst);
        assert_eq!(ship.manhattan_distance(), 25);

        let mut ship = Ship::default();
        ship.follow_instructions_with_waypoint(&inst);
        assert_eq!(ship.manhattan_distance(), 286)
    }
}
