use advent_of_code::Solver;
use nom::character::complete::{alpha1, digit1, line_ending, space1};
use nom::combinator::{map, map_res};
use nom::multi::separated_list1;
use nom::sequence::separated_pair;
use nom::IResult;
use std::collections::HashSet;
use std::ops::{AddAssign, Sub};
use std::str::FromStr;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day09.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

#[derive(Debug)]
enum Direction {
    Up,
    Left,
    Right,
    Down,
}
impl Direction {
    fn to_vec(&self) -> Pos {
        match self {
            Direction::Up => pos(0, 1),
            Direction::Left => pos(-1, 0),
            Direction::Right => pos(1, 0),
            Direction::Down => pos(0, -1),
        }
    }
}

impl FromStr for Direction {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "R" => Ok(Self::Right),
            "L" => Ok(Self::Left),
            "D" => Ok(Self::Down),
            "U" => Ok(Self::Up),
            _ => Err(()),
        }
    }
}

#[derive(Debug)]
struct Command(Direction, usize);

type Input = Vec<Command>;

fn first_solution(input: &Input) -> usize {
    let mut head_position = pos(0, 0);
    let mut tail_position = pos(0, 0);

    let mut used_tail_position = HashSet::new();
    used_tail_position.insert(tail_position);

    for Command(d, n) in input {
        for _ in 0..*n {
            let vec = d.to_vec();
            head_position += vec;

            tail_position = follow(head_position, tail_position);
            used_tail_position.insert(tail_position);
            // dbg_head_tails(head_position, tail_position);
        }
    }

    // dbg_movements(&used_tail_position);
    used_tail_position.len()
}

// fn dbg_head_tails(head: Pos, tail: Pos) {
//     println!("----------");
//     for y in (0..7).rev() {
//         for x in 0..7 {
//             let pos = pos(x, y);
//             if head == pos {
//                 print!("H");
//             } else if tail == pos {
//                 print!("T");
//             } else if pos == (Pos { x: 0, y: 0 }) {
//                 print!("s")
//             } else {
//                 print!(".")
//             }
//         }
//         println!()
//     }
// }
// fn dbg_movements(used_pos: &HashSet<Pos>) {
//     println!("----------");
//     for y in (0..5).rev() {
//         for x in 0..5 {
//             if used_pos.contains(&pos(x, y)) {
//                 print!("#");
//             } else {
//                 print!(" ");
//             }
//         }
//         println!()
//     }
// }

fn second_solution(input: &Input) -> usize {
    let mut knots = [pos(0, 0); 10];

    let mut used_tail_position = HashSet::new();
    used_tail_position.insert(knots[9]);

    for Command(d, n) in input {
        for _ in 0..*n {
            let vec = d.to_vec();
            knots[0] += vec;

            for i in 1..10 {
                knots[i] = follow(knots[i - 1], knots[i]);
            }
            used_tail_position.insert(knots[9]);
        }
    }
    used_tail_position.len()
}

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone)]
struct Pos {
    x: i32,
    y: i32,
}
impl AddAssign for Pos {
    fn add_assign(&mut self, rhs: Self) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}
impl Sub for Pos {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

fn pos(x: i32, y: i32) -> Pos {
    Pos { x, y }
}
fn manhattan(a: Pos, b: Pos) -> i32 {
    (a.x - b.x).abs() + (a.y - b.y).abs()
}

fn follow(head: Pos, tail: Pos) -> Pos {
    let mut tail = tail;
    let dist = manhattan(head, tail);
    if dist > 1 {
        if tail.x == head.x {
            tail.y += (head.y - tail.y).signum();
        } else if tail.y == head.y {
            tail.x += (head.x - tail.x).signum();
        } else if dist > 2 {
            tail.y += (head.y - tail.y).signum();
            tail.x += (head.x - tail.x).signum();
        }
    }
    tail
}

// ------------------

fn parse_command(input: &str) -> IResult<&str, Command> {
    map(
        separated_pair(
            map_res(alpha1, str::parse::<Direction>),
            space1,
            map_res(digit1, str::parse::<usize>),
        ),
        |(d, n)| Command(d, n),
    )(input)
}

fn parser(input: &str) -> IResult<&str, Input> {
    separated_list1(line_ending, parse_command)(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn manhattan_dist() {
        assert_eq!(manhattan(pos(0, 0), pos(0, 0)), 0);
        assert_eq!(manhattan(pos(0, 0), pos(1, 0)), 1);
        assert_eq!(manhattan(pos(0, 0), pos(0, 1)), 1);
        assert_eq!(manhattan(pos(0, 0), pos(0, -1)), 1);
        assert_eq!(manhattan(pos(0, 0), pos(-1, 0)), 1);
        assert_eq!(manhattan(pos(0, 0), pos(2, 0)), 2);
        assert_eq!(manhattan(pos(0, 0), pos(1, 1)), 2);
        assert_eq!(manhattan(pos(0, 0), pos(1, 2)), 3);
    }

    #[test]
    fn example() {
        let input = r#"R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2"#;
        let first_answer = 13;
        let second_answer = 1;

        let parsed = parser(input).unwrap().1;

        assert_eq!(first_solution(&parsed), first_answer);
        assert_eq!(second_solution(&parsed), second_answer)
    }

    #[test]
    fn second_example() {
        let input = r#"R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20"#;
        let second_answer = 36;

        let parsed = parser(input).unwrap().1;

        assert_eq!(second_solution(&parsed), second_answer)
    }
}
