#![allow(
    clippy::cast_possible_wrap,
    clippy::cast_possible_truncation,
    clippy::needless_range_loop
)]
use advent_of_code::Solver;
use nom::character::complete::{digit1, line_ending};
use nom::combinator::map;
use nom::multi::separated_list1;
use nom::IResult;
use std::fmt::{Debug, Formatter};
use std::ops::Index;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day08.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

struct Forest {
    rows: Vec<Vec<u8>>,
}
impl Debug for Forest {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for row in &self.rows {
            for tree in row {
                write!(f, "{}", tree)?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

impl Index<usize> for Forest {
    type Output = Vec<u8>;

    fn index(&self, index: usize) -> &Self::Output {
        &self.rows[index]
    }
}

type Input = Forest;

fn first_solution(input: &Input) -> usize {
    let num_columns = input.rows[0].len();

    let mut visibility: Vec<Vec<bool>> = Vec::with_capacity(input.rows.len());
    for row in &input.rows {
        visibility.push(vec![false; row.len()]);
    }

    // Check visibility from top
    for x in 0..num_columns {
        let mut highest = -1;
        for (y, row) in input.rows.iter().enumerate() {
            let tree = row[x] as i8;
            if tree > highest {
                visibility[y][x] = true;
                highest = tree;
            }
        }
    }
    // Check visibility from bottom
    for x in 0..num_columns {
        let mut highest = -1;
        for (y, row) in input.rows.iter().enumerate().rev() {
            let tree = row[x] as i8;
            if tree > highest {
                visibility[y][x] = true;
                highest = tree;
            }
        }
    }
    // Check visibility from left
    for (y, row) in input.rows.iter().enumerate() {
        let mut highest = -1;
        for x in 0..num_columns {
            let tree = row[x] as i8;
            if tree > highest {
                visibility[y][x] = true;
                highest = tree;
            }
        }
    }
    // Check visibility from right
    for (y, row) in input.rows.iter().enumerate() {
        let mut highest = -1;
        for x in (0..num_columns).rev() {
            let tree = row[x] as i8;
            if tree > highest {
                visibility[y][x] = true;
                highest = tree;
            }
        }
    }

    visibility.iter().flatten().filter(|b| **b).count()
}

fn second_solution(input: &Input) -> usize {
    let num_rows = input.rows.len();
    let num_cols = input.rows[0].len();

    let mut highest_score = 0;
    for (y, row) in input.rows.iter().enumerate() {
        for (x, tree) in row.iter().enumerate() {
            let tree = *tree;

            // Check to the left
            let mut to_the_left = 0;
            for xx in (0..x).rev() {
                to_the_left += 1;
                if input.rows[y][xx] >= tree {
                    break;
                }
            }
            // Check to the right
            let mut to_the_right = 0;
            for xx in (x + 1)..num_cols {
                to_the_right += 1;
                if input.rows[y][xx] >= tree {
                    break;
                }
            }
            // Check above
            let mut to_above = 0;
            for yy in (0..y).rev() {
                to_above += 1;
                if input.rows[yy][x] >= tree {
                    break;
                }
            }
            // Check below
            let mut to_below = 0;
            for yy in (y + 1)..num_rows {
                to_below += 1;
                if input.rows[yy][x] >= tree {
                    break;
                }
            }

            let score = to_the_left * to_the_right * to_above * to_below;
            println!(
                "{:?}\n\t{} = {} {} {} {}",
                (x, y),
                score,
                to_the_left,
                to_the_right,
                to_above,
                to_below
            );
            if highest_score < score {
                highest_score = score;
            }
        }
    }
    highest_score
}

fn parse_row(input: &str) -> IResult<&str, Vec<u8>> {
    map(digit1, |a: &str| {
        a.chars().map(|c| c.to_digit(10).unwrap() as u8).collect()
    })(input)
}

fn parser(input: &str) -> IResult<&str, Input> {
    map(separated_list1(line_ending, parse_row), |rows| Forest {
        rows,
    })(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn example() {
        let input = r#"30373
25512
65332
33549
35390"#;
        let first_answer = 21;
        let second_answer = 8;

        let parsed = parser(input).unwrap().1;
        assert_eq!(first_solution(&parsed), first_answer);
        assert_eq!(second_solution(&parsed), second_answer)
    }
}
