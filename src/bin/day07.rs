use advent_of_code::Solver;
use nom::branch::alt;
use nom::bytes::complete::tag;
use nom::character::complete::{alphanumeric1, digit1, line_ending, space1};
use nom::combinator::{map, map_res};
use nom::multi::{many1, separated_list0, separated_list1};
use nom::sequence::separated_pair;
use nom::IResult;
use std::fmt::{Debug, Formatter};
use std::ops::{Index, IndexMut};

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day07.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

type Dir = String;

#[derive(Debug, Eq, PartialEq)]
struct File {
    name: String,
    size: usize,
}

#[derive(Eq, PartialEq)]
enum LsItem {
    File(File),
    Dir(Dir),
}

impl Debug for LsItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            LsItem::File(file) => {
                write!(f, "{} {}", file.size, file.name)
            }
            LsItem::Dir(d) => {
                write!(f, "dir {}", d)
            }
        }
    }
}

#[derive(Eq, PartialEq)]
enum Command {
    Cd(Dir),
    Ls(Vec<LsItem>),
}

impl Debug for Command {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "$ ")?;
        match self {
            Command::Cd(dir) => {
                write!(f, "cd {}", dir)
            }
            Command::Ls(ls) => {
                write!(f, "ls\n{:?}", ls)
            }
        }
    }
}

type Input = Vec<Command>;

//---------- DATA STRUCTURE -------------------

pub struct TreeNode {
    name: String,
    size: Option<usize>,
    is_dir: bool,

    children_id: Vec<usize>,
    parent_id: Option<usize>,
}

pub struct Tree {
    nodes: Vec<TreeNode>,
}

impl Default for Tree {
    fn default() -> Self {
        let root = TreeNode {
            name: "/".into(),
            size: None,
            is_dir: true,
            children_id: vec![],
            parent_id: None,
        };

        Self { nodes: vec![root] }
    }
}
impl Index<usize> for Tree {
    type Output = TreeNode;

    fn index(&self, index: usize) -> &Self::Output {
        &self.nodes[index]
    }
}

impl IndexMut<usize> for Tree {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.nodes[index]
    }
}

impl Debug for Tree {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut stack = vec![(0, 0)];
        while let Some((id, indentation)) = stack.pop() {
            let node = &self[id];
            for _ in 0..indentation {
                write!(f, " ")?;
            }
            writeln!(f, "- {} {} {:?}", node.name, node.is_dir, node.size)?;

            for child in node.children_id.iter().rev() {
                stack.push((*child, indentation + 1));
            }
        }
        Ok(())
    }
}

impl Tree {
    fn build(commands: &[Command]) -> Self {
        let mut tree: Tree = Tree::default();
        let mut cursor = 0;

        for command in commands {
            match command {
                Command::Cd(dir) => match dir.as_str() {
                    "/" => {
                        cursor = 0;
                    }
                    ".." => {
                        cursor = tree[cursor].parent_id.unwrap_or(0);
                    }
                    dir => {
                        cursor = tree[cursor]
                            .children_id
                            .iter()
                            .map(|id| (*id, &tree[*id]))
                            .find(|(_, node)| node.is_dir && node.name == dir)
                            .unwrap()
                            .0;
                    }
                },
                Command::Ls(elements) => {
                    for item in elements {
                        match item {
                            LsItem::File(file) => tree.add_file_to(cursor, file),
                            LsItem::Dir(dir) => tree.add_dir_to(cursor, dir),
                        }
                    }
                }
            }
        }

        tree
    }

    fn add_file_to(&mut self, parent_id: usize, file: &File) {
        self.add_node_to(parent_id, file.name.clone(), Some(file.size), false);
    }
    fn add_dir_to(&mut self, parent_id: usize, dir: &Dir) {
        self.add_node_to(parent_id, dir.clone(), None, true);
    }

    fn add_node_to(&mut self, parent_id: usize, name: String, size: Option<usize>, is_dir: bool) {
        let new_id = self.nodes.len();
        let node = TreeNode {
            name,
            size,
            is_dir,
            children_id: vec![],
            parent_id: Some(parent_id),
        };
        self.nodes.push(node);
        self.nodes[parent_id].children_id.push(new_id);
    }

    fn compute_dir_sizes(&mut self) {
        self.nodes[0].size = Some(self.rec_compute_dir_sizes(0));
    }
    fn rec_compute_dir_sizes(&mut self, cursor: usize) -> usize {
        if self[cursor].is_dir {
            let childrens = self[cursor].children_id.clone();
            let size = childrens
                .iter()
                .map(|id| self.rec_compute_dir_sizes(*id))
                .sum();
            self[cursor].size = Some(size);
        }
        self[cursor].size.unwrap()
    }

    fn sum_first_solution(&self) -> usize {
        self.nodes
            .iter()
            .filter(|node| node.is_dir && node.size.unwrap() <= 100_000)
            .map(|node| node.size.unwrap())
            .sum()
    }

    fn find_smallest_dir_to_delete(&self, needed_space: usize) -> usize {
        self.nodes
            .iter()
            .filter(|node| node.is_dir && node.size.unwrap() >= needed_space)
            .map(|node| node.size.unwrap())
            .min()
            .unwrap()
    }
}

fn first_solution(input: &Input) -> usize {
    let mut tree = Tree::build(input);
    tree.compute_dir_sizes();
    tree.sum_first_solution()
}

fn second_solution(input: &Input) -> usize {
    let mut tree = Tree::build(input);
    tree.compute_dir_sizes();
    let total_space = 70_000_000;
    let update_space = 30_000_000;

    let used_space = tree.nodes[0].size.unwrap();

    let unused_space = total_space - used_space;

    tree.find_smallest_dir_to_delete(update_space - unused_space)
}

// -------------- PARSING -------------------------

fn parser(input: &str) -> IResult<&str, Input> {
    separated_list1(line_ending, parse_command)(input)
}

fn parse_dir_name(input: &str) -> IResult<&str, Dir> {
    map(
        many1(alt((alphanumeric1, tag("."), tag("/")))),
        |x: Vec<&str>| x.join(""),
    )(input)
}

fn parse_cd(input: &str) -> IResult<&str, Command> {
    map(separated_pair(tag("cd"), space1, parse_dir_name), |x| {
        Command::Cd(x.1)
    })(input)
}

fn parse_file(input: &str) -> IResult<&str, File> {
    map_res(
        separated_pair(digit1, space1, parse_dir_name),
        |(size, name)| size.parse().map(|size| File { name, size }),
    )(input)
}

fn parse_ls_item(input: &str) -> IResult<&str, LsItem> {
    let parse_dir_item = map(separated_pair(tag("dir"), space1, parse_dir_name), |x| x.1);
    alt((
        map(parse_file, LsItem::File),
        map(parse_dir_item, LsItem::Dir),
    ))(input)
}

fn parse_ls(input: &str) -> IResult<&str, Command> {
    map(
        separated_pair(
            tag("ls"),
            line_ending,
            separated_list0(line_ending, parse_ls_item),
        ),
        |x| Command::Ls(x.1),
    )(input)
}

fn parse_command(input: &str) -> IResult<&str, Command> {
    map(
        separated_pair(tag("$"), space1, alt((parse_ls, parse_cd))),
        |x| x.1,
    )(input)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn parsing() {
        assert_eq!(parse_cd("cd asd").unwrap().1, Command::Cd("asd".into()));
        assert_eq!(parse_cd("cd ..").unwrap().1, Command::Cd("..".into()));
        assert_eq!(parse_cd("cd /").unwrap().1, Command::Cd("/".into()));
        assert_eq!(
            parse_cd("cd folder/file").unwrap().1,
            Command::Cd("folder/file".into())
        );
    }

    #[test]
    fn example() {
        let input = r#"$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k"#;
        let first_answer = 95437;
        let second_answer = 24933642;

        let parsed = parser(input).unwrap().1;

        assert_eq!(first_solution(&parsed), first_answer);
        assert_eq!(second_solution(&parsed), second_answer)
    }
}
