use advent_of_code::Solver;
use nom::character::complete::{alpha1, line_ending, space1};
use nom::combinator::map_res;
use std::str::FromStr;

use nom::multi::separated_list1;
use nom::sequence::separated_pair;
use nom::IResult;

fn main() -> anyhow::Result<()> {
    Solver::with_input("res/day2.txt")
        .with_parser(parser)
        .with_first_solution(first_solution)
        .with_second_solution(second_solution)
        .solve()
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Symbol {
    Rock,
    Paper,
    Scissor,
}

impl FromStr for Symbol {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" | "X" => Ok(Self::Rock),
            "B" | "Y" => Ok(Self::Paper),
            "C" | "Z" => Ok(Self::Scissor),
            _ => Err(()),
        }
    }
}

impl Symbol {
    fn score(self) -> usize {
        match self {
            Symbol::Rock => 1,
            Symbol::Paper => 2,
            Symbol::Scissor => 3,
        }
    }
    fn check((x, y): Strategy) -> usize {
        match (x, y) {
            (Self::Rock, Self::Scissor)
            | (Self::Scissor, Self::Paper)
            | (Self::Paper, Self::Rock) => 6,
            (x, y) if x == y => 3,
            _ => 0,
        }
    }

    fn get_symbol(self, strategy: Symbol) -> Symbol {
        match strategy {
            // Lose
            Symbol::Rock => match &self {
                Symbol::Rock => Symbol::Scissor,
                Symbol::Paper => Symbol::Rock,
                Symbol::Scissor => Symbol::Paper,
            },
            // Draw
            Symbol::Paper => self,
            // Win
            Symbol::Scissor => match &self {
                Symbol::Rock => Symbol::Paper,
                Symbol::Paper => Symbol::Scissor,
                Symbol::Scissor => Symbol::Rock,
            },
        }
    }
}

type Strategy = (Symbol, Symbol);
type Input = Vec<Strategy>;

fn first_solution(input: &Input) -> usize {
    input
        .iter()
        .map(|(them, me)| me.score() + Symbol::check((*me, *them)))
        .sum()
}

fn second_solution(input: &Input) -> usize {
    input
        .iter()
        .map(|(them, action)| {
            let me = them.get_symbol(*action);
            me.score() + Symbol::check((me, *them))
        })
        .sum()
}

fn parse_symbol(input: &str) -> IResult<&str, Symbol> {
    map_res(alpha1, str::parse::<Symbol>)(input)
}

fn parser(input: &str) -> IResult<&str, Input> {
    separated_list1(
        line_ending,
        separated_pair(&mut parse_symbol, space1, &mut parse_symbol),
    )(input)
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn parse() {
        assert_eq!("A".parse::<Symbol>().unwrap(), Symbol::Rock)
    }

    #[test]
    fn self_score() {
        assert_eq!(Symbol::Rock.score(), 1);
        assert_eq!(Symbol::Paper.score(), 2);
        assert_eq!(Symbol::Scissor.score(), 3);
    }

    #[test]
    fn score() {
        let cases = [
            ("A X", 3),
            ("B Y", 3),
            ("C Z", 3),
            ("A Y", 0),
            ("B Z", 0),
            ("C X", 0),
            ("A Z", 6),
            ("B X", 6),
            ("C Y", 6),
        ];

        for (case, score) in cases {
            let parsed = parser(case).unwrap().1[0];
            println!("{}", case);
            assert_eq!(Symbol::check(parsed), score);
        }
    }

    #[test]
    fn example() {
        let input = r#"A Y
B X
C Z"#;
        let parsed = parser(input).unwrap().1;

        assert_eq!(first_solution(&parsed), 15);
        let first_line = vec![parsed[0].clone()];
        let second_line = vec![parsed[1].clone()];
        let third_line = vec![parsed[2].clone()];
        assert_eq!(first_solution(&first_line), 8);
        assert_eq!(first_solution(&second_line), 1);
        assert_eq!(first_solution(&third_line), 6);
    }
}
